var express = require('express');
var router = new express.Router();

const db = [
{
	id : 0,
	titre: "T BO JUJU",
},
{
	id : 1,
	titre: "Princess JUJU",
}
];


//recuperer tous les films
router.get("films",function(req,res){
	res.send(db); //envoie la db
});

//retrouver un film par ID 
router.get("/:id",function(req,res){
	const {id} = req.params; //recuperer l'id demand�'
	const titre = _.find(db,["id",id]); //recherche dans la db l'id qui correspond
	res.status(200).json({
	message: 'Film trouv�!', titre
	});
});

//ajouter un nouveau film 
router.put('/',function(req,res)
{
const {film} = req.body; //recupere les donn�es
const id = _.uniqueID(); //creation d'un iD unique
db.push({film,id}); //mettre le tout dans un array 
res.json({message: 'ajout de ${id}', titre: {Film,id}}); //message
});


//mise � jour film
router.post('/id',(req,res){
const {id} = req.params; //r�cup�re l'id souhait�
const {film} = req.body;  //r�cup�re les infos du films
const filmToUpdate = _.find(db,["id",id]); // trouver le bon Film
filmToUpdate.film = film; //l'update
res.json({message: 'Update termin�'});
});

//supprimer
router.delete('/:id',(req,res){
const {id} = req.params; //recup ID
_.remove(db,["id",id]);//supprimer
res.json({message: "C'est supprim�"});
});

module.exports = router;
